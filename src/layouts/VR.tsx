import React, { FunctionComponent } from 'react'
import { VRCanvas, DefaultXRControllers, Hands } from '@react-three/xr'
import { Sky, OrbitControls } from '@react-three/drei'
import { Physics } from '@react-three/cannon'

import { Floor } from '../components/Canvas/Floor'

export const VRLayout: FunctionComponent = ({ children }) => (
  <VRCanvas
    style={{
      height: '100vh',
      width: '100vw',
    }}
  >
    <Physics>
      <OrbitControls />
      <DefaultXRControllers />
      <Hands />

      <ambientLight intensity={0.5} />
      <spotLight position={[1, 8, 1]} angle={0.3} penumbra={1} intensity={1} castShadow />

      <Sky />
      {children}
      <Floor color="rebeccapurple" />
    </Physics>
  </VRCanvas>
)
