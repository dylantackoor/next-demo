import React, { FunctionComponent } from 'react'

import { InboxItem, InboxItemProps, defaultProps as ItemDefaultProps } from '../InboxItem'

export interface InboxListProps {
  items: InboxItemProps[]
}

export const defaultProps: InboxListProps = {
  items: [ItemDefaultProps],
}

export const InboxList: FunctionComponent<InboxListProps> = ({ items } = defaultProps) => (
  <div>
    <ul>
      {items.map(item => (
        <InboxItem {...item} key={item.key} />
      ))}
    </ul>
  </div>
)
