import React from 'react'
import { Story, Meta } from '@storybook/react'

import { InboxList, InboxListProps, defaultProps } from './index'

export default {
  title: 'Components/Inbox/List',
  component: InboxList,
  parameters: {
    jest: ['InboxList.spec.tsx'],
    a11y: { disable: false },
  },
  argTypes: {
    textColor: { control: 'color' },
  },
} as Meta

const Template: Story<InboxListProps> = args => <InboxList {...args} />

export const Default = Template.bind({})
Default.args = defaultProps
