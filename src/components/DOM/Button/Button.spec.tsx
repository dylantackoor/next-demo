import React from 'react'
import { render } from '@testing-library/react'

import { Button, defaultProps } from './index'

describe('<Button />', () => {
  it('should render Default Buttons', () => {
    render(<Button {...defaultProps} />)
  })

  it('should render Disabled Buttons', () => {
    render(<Button {...defaultProps} variant="disabled" />)
  })

  it('should render Icon Buttons', () => {
    render(<Button {...defaultProps} variant="icon" />)
  })

  it('should render File Buttons', () => {
    render(<Button {...defaultProps} variant="file" />)
  })
})
