import React, { FunctionComponent } from 'react'

export interface ButtonProps {
  displayText: string
  ariaLabel?: string
  onPress: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
  variant?: 'primary' | 'disabled' | 'icon' | 'file'
  textColor?: string
}

export const defaultProps: ButtonProps = {
  displayText: 'Lorem Ipsum',
  onPress: () => alert('Button pressed'),
  ariaLabel: 'Empty Button',
  textColor: '#000',
}

export const Button: FunctionComponent<ButtonProps> = ({
  displayText,
  onPress,
  ariaLabel,
  textColor,
} = defaultProps) => (
  <div aria-label={ariaLabel} onClick={onPress}>
    <p style={{ color: textColor }}>{displayText}</p>
  </div>
)
