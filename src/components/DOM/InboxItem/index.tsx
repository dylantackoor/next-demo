import React, { FunctionComponent } from 'react'

export interface InboxItemProps {
  key: number
  timestamp: Date
  ariaLabel?: string
  onClick: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void

  broker: string
  carrier: string
  status: string // convert to enum
}

export const defaultProps: InboxItemProps = {
  broker: 'Inbox Item',
  carrier: '',
  status: '',
  onClick: () => alert('Item Selected'),
  ariaLabel: 'Inbox Item',
  key: 1,
  timestamp: new Date(),
}

export const InboxItem: FunctionComponent<InboxItemProps> = ({
  broker,
  carrier,
  status,
  onClick,
  ariaLabel,
} = defaultProps) => (
  <div aria-label={ariaLabel} onClick={onClick}>
    <p>{broker}</p>
    <p>{carrier}</p>
    <p>{status}</p>
  </div>
)
