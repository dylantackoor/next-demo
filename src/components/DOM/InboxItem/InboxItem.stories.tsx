import React from 'react'
import { Story, Meta } from '@storybook/react'

import { InboxItem, InboxItemProps, defaultProps } from './index'

export default {
  title: 'Components/Inbox/Item',
  component: InboxItem,
  parameters: {
    jest: ['InboxItem.spec.tsx'],
    a11y: { disable: false },
  },
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta

const Template: Story<InboxItemProps> = args => <InboxItem {...args} />

export const Example = Template.bind({})
Example.args = defaultProps
