import React, { FunctionComponent } from 'react'
import { usePlane, PlaneProps } from '@react-three/cannon'

export interface FloorProps extends PlaneProps {
  color: string
}

export const Floor: FunctionComponent<FloorProps> = props => {
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], ...props }))

  return (
    <mesh ref={ref} receiveShadow>
      <meshStandardMaterial attach="material" color={props.color} />
      <planeBufferGeometry attach="geometry" args={[40, 40]} />
    </mesh>
  )
}
