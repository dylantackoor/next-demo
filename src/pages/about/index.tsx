import React from 'react'
import { NextPage } from 'next'
import { Organization } from 'schema-dts'
import { jsonLdScriptProps } from 'react-schemaorg'

import { SEO } from '../../components/DOM/SEO'

const AboutPage: NextPage = () => (
  <div>
    <SEO title="About">
      <script
        {...jsonLdScriptProps<Organization>({
          '@context': 'https://schema.org',
          '@type': 'Organization',
          name: 'Big Rig Dispatchers',
          legalName: 'Big Rig Dispatchers',
          url: 'https://www.bigrigdispatchers.com',
          logo: 'https://www.bigrigdispatchers.com/wp-content/uploads/2018/04/BigRig_White-Red.png',
          // foundingDate: '2009',
          founders: [
            {
              '@type': 'Person',
              name: 'Steve',
            },
            {
              '@type': 'Person',
              name: 'Adams',
            },
            {
              '@type': 'Person',
              name: 'Zef',
            },
          ],
          address: {
            '@type': 'PostalAddress',
            streetAddress: '9777 Satellite Blvd 160',
            addressLocality: 'Orlando',
            addressRegion: 'FL',
            postalCode: '34787',
            addressCountry: 'USA',
          },
          contactPoint: {
            '@type': 'ContactPoint',
            contactType: 'customer support',
            telephone: '[+305-407-8255]',
            // email: 'info@elite-strategies.com',
          },
          sameAs: [
            'https://www.facebook.com/bigrigdispatchers',
            'https://www.bigrigdispatchservices.com/',
          ],
        })}
      />
    </SEO>
    <p>About</p>
  </div>
)

export default AboutPage
