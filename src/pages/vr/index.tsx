import React, { FunctionComponent } from 'react'
import { Text } from '@react-three/drei'
import { useBox, BoxProps } from '@react-three/cannon'

import { VRLayout } from '../../layouts/VR'

const Cube: FunctionComponent<BoxProps> = props => {
  const [ref] = useBox(() => ({ mass: 1, position: [0, 5, 0], ...props }))
  return (
    <mesh ref={ref} receiveShadow>
      <boxBufferGeometry attach="geometry" />
    </mesh>
  )
}

const WebXRPage: FunctionComponent = () => (
  <VRLayout>
    <Text position={[0, 0.8, -1]} fontSize={0.05} color="#000" anchorX="center" anchorY="middle">
      Hello react-xr!
    </Text>
    <Cube />
  </VRLayout>
)

export default WebXRPage
