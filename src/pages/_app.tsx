import React, { FunctionComponent } from 'react'
import { AppProps, NextWebVitalsMetric } from 'next/app'
import Head from 'next/head'

import '../styles/global.css'

const App: FunctionComponent<AppProps> = ({ Component, pageProps }) => (
  <>
    <Head>
      <meta charSet="utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta
        name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
      />
      <title>BigRig</title>
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <link rel="manifest" href="/manifest.json" />
      <link href="/icons/favicon-16x16.png" rel="icon" type="image/png" sizes="16x16" />
      <link href="/icons/favicon-32x32.png" rel="icon" type="image/png" sizes="32x32" />
      <link rel="apple-touch-icon" href="/apple-icon.png"></link>
      <meta name="theme-color" content="#317EFB" />
    </Head>
    <Component {...pageProps} />
  </>
)

export default App

export const reportWebVitals: (metric: NextWebVitalsMetric) => void = metric => {
  switch (metric.name) {
    case 'FCP':
      console.log('TODO: send FCP to some API', metric)
      break
    case 'LCP':
      console.log('TODO: send LCP to some API', metric)
      break
    case 'CLS':
      console.log('TODO: send CLS to some API', metric)
      break
    case 'FID':
      console.log('TODO: send FID to some API', metric)
      break
    case 'TTFB':
      console.log('TODO: send TTFB to some API', metric)
      break
    default:
      break
  }
}
