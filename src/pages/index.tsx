import React from 'react'
import { NextPage } from 'next'

const HomePage: NextPage = () => (
  <div>
    <p>Hello World</p>
  </div>
)

export default HomePage
