# www.bigrig.dev

[Storybook Deployment URL](https://www-bigrig-dev.vercel.app/?path=/story/example-introduction--page)

## Initial Setup

```sh
npm i
```

If you have VSCode, install the suggested extensions

## Component/Page Development

```sh
npm run storybook
```

See Button component for example

## App Development

To start a debug version of the react app,

```sh
npm dev
```

If you have VSCode, run `Next: Full` instead to get started w/debugging
