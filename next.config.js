/* eslint-disable @typescript-eslint/no-var-requires */

const { GuessPlugin } = require('guess-webpack')
const withTM = require('next-transpile-modules')(['three', '@react-three/xr', '@react-three/drei']) // pass the modules you would like to see transpiled
const withImages = require('next-images')
// const withPWA = require('next-pwa')
// const runtimeCaching = require('next-pwa/cache')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

const { GOOGLE_ANALYTICS_VIEW_ID } = process.env

const nextOptions = {
  reactStrictMode: true,
  poweredByHeader: false,
  productionBrowserSourceMaps: true,
  webpack: (config, { isServer }) => {
    if (isServer || !GOOGLE_ANALYTICS_VIEW_ID) return config
    config.plugins.push(new GuessPlugin({ GA: GOOGLE_ANALYTICS_VIEW_ID }))
    return config
  },
  // pwa: {
  //   // disable: process.env.NODE_ENV === 'development',
  //   dest: 'public',
  //   runtimeCaching,
  // },
}

module.exports = withTM(withImages(withBundleAnalyzer(nextOptions)))
