import { addons } from '@storybook/addons'

import { BigRigStorybookTheme } from './theme'

addons.setConfig({
  theme: BigRigStorybookTheme,
})
