import { create } from '@storybook/theming/create'

export const BigRigStorybookTheme = create({
  base: 'light',
  brandTitle: 'BigRig UI Library',
  brandUrl: 'https://www.bigrigdispatchers.com',
  brandImage: 'https://www.bigrigdispatchers.com/wp-content/uploads/2018/04/BigRig_White-Red.png',
})
