import { withTests } from '@storybook/addon-jest'
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks'
import { MINIMAL_VIEWPORTS, INITIAL_VIEWPORTS } from '@storybook/addon-viewport'

import results from '../.jest-test-results.json'

// const IS_PRODUCTION = process.env.CI

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  viewport: {
    viewports: { ...MINIMAL_VIEWPORTS, ...INITIAL_VIEWPORTS },
  },
  docs: {
    container: DocsContainer,
    page: DocsPage,
  },
}

export const decorators = [withTests({ results })]
